<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/***************************/
/* Routes pour le site web */
/***************************/

// Index page
Route::get('/', 'DatabaseController@index');
Route::get('/index.php', 'DatabaseController@index');

// Home page
Route::get('/home.php', array('before' => 'auth', 'uses' => 'DatabaseController@home'));

// Search page
Route::get('/search.php', array('before' => 'auth', 'uses' => 'HomeController@search'));

// Return page
Route::get('/returnbook.php', array('before' => 'auth', 'uses' => 'DatabaseController@returnbook'));

// Donnation page
Route::get('/donnate.php', array('before' => 'auth', 'uses' => 'HomeController@donnate'));

// Page to modify the book
Route::post('/editbook.php', array('before' => 'auth', 'uses' => 'DatabaseController@editbook'));

// Script adding a book in the database after submission
Route::post('/addbook.php', array('before' => 'auth', 'uses' => 'DatabaseController@addbook'));

// Script which modifies the book
Route::post('/applychanges.php', array('before' => 'auth','uses' =>  'DatabaseController@applychanges'));

// Page to delete a book
Route::get('/delete.php', array('before' => 'auth', 'uses' => 'DatabaseController@deleteget'));
Route::post('/delete.php', array('before' => 'auth', 'uses' => 'DatabaseController@deletepost'));

// Script which returns the search results
Route::post('/searchbook.php', array('before' => 'auth', 'uses' => 'DatabaseController@searchbook'));

// Page to login
Route::get('/login.php', 'AuthentificationController@loginGet');
Route::post('/login.php', 'AuthentificationController@loginPost');

// Page to sign up
route::get('/signup.php','AuthentificationController@signupGet');
route::post('/signup.php','AuthentificationController@signupPost');

// Page to logout
route::get('/logout.php', 'AuthentificationController@logout');





/**************************/
/* Routes pour l'API REST */
/**************************/

// Groupe de routes pour le versioning d'API
Route::group(array('prefix' => 'api', 'before' => 'auth.basic'), function() {
	Route::resource('v1', 'APIController');
});
