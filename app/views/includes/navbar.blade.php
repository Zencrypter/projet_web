<div class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">Accueil</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="home.php">Home</a></li>
				<li><a href="search.php">Rechercher et emprunter</a></li>
				<li><a href="returnbook.php">Retour</a></li>
				<li><a href="donnate.php">Don</a></li>
			@if(Auth::check())
				@if(Auth::user()->username == 'admin')
					<li><a href="delete.php">Supprimer</a></li>
				@endif
			@endif
			</ul>
			<ul class="nav navbar-nav navbar-right">
			@if(Auth::check())
				<li><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{ Auth::user()->username }}</a></li>
				<li>
					<form method="GET" action="logout.php">
						<button type="submit" class="btn btn-primary navbar-btn">Logout</button>
					</form>
				</li>
			@else
				<li>
					<form method="GET" action="login.php">
						<button type="submit" class="btn btn-success navbar-btn">Login</button>
					</form>
				</li>
			@endif
			</ul>
		</div><!--/.nav-collapse -->
	</div><!--/.container-fluid -->
</div>
&nbsp;
