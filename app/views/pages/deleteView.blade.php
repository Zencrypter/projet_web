<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
	@include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Supprimer un livre</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Supprimer un livre</h1>
	</div>

	<!-- Navigation Menu (navbar) -->
	@include('includes.navbar')
	
	&nbsp;

	<!-- Display the available books -->
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">Livres actuellement à la bibliothèque :</div>

		<!-- Table -->
		<table class="table">
			<thead>
				<tr>
				<th>Titre</th>
				<th>Nom de l'auteur</th>
				<th>Edition</th>
				<th>Genre</th>
				<th>Supprimer</th>
				</tr>
			</thead>
			<tbody>
			
			@foreach($books as $book)
			<tr>
				<td>{{ $book->title }}</td>
				<td>{{ $book->author_name }}</td>
				<td>{{ $book->edition }}</td>
				<td>{{ $book->gender }}</td>
				<td>
					<form method="POST" action="/delete.php">
						<input type="hidden" name="inputID" value="{{ $book->id }}">
						<button type="submit" class="btn btn-danger navbar-btn">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</button>
					</form>
				</td>
			</tr>
			@endforeach
			
			</tbody>
		</table>

	</div>
	
</div>
</body>
</html>
