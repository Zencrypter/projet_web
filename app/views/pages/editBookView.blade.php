<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
    @include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Modifier un livre</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Modifier un livre</h1>
	</div>

	<!-- Navigation Menu (navbar) -->
	@include('includes.navbar')

	<!-- Little paragraph -->
	<p>Les références du livre ont été mal entrées ?</p>
	<p>Modifiez "{{ $book->title }}" dès maintenant ci-dessous !</p>
	
	&nbsp;

	<!-- Form to edit the book -->
	<form method="POST" action="applychanges.php">
	<input type="hidden" name="inputID" value="{{ $id }}">
	<div class="input-group">
  		<span class="input-group-addon">Titre</span>
  		<input type="text" class="form-control" name="inputTitle" value="{{ $book->title }}">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Auteur</span>
  		<input type="text" class="form-control" name="inputAuthor" value="{{ $book->author_name }}">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Edition</span>
  		<input type="text" class="form-control" name="inputEdition" value="{{ $book->edition }}">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Type</span>
  		<input type="text" class="form-control" name="inputGender" value="{{ $book->gender }}">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Date de publication</span>
  		<input type="text" class="form-control" name="inputDate" value="{{ $book->publication_date }}">
	</div>
	&nbsp;
	<div>
		<button type="submit" class="btn btn-primary">Modifier !</button>
	</div>
	</form>

</div>
</body>
</html>
