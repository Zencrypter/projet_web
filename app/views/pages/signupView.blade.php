<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
    @include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Identification</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Créer un compte</h1>
	</div>
	@if($var == 0)
	<div class="alert alert-danger" role="alert">
		<strong>Erreur !</strong> Les 2 mots de passe ne sont pas identiques.
	</div>
	@endif
	<!-- Form to sign up -->
	<form method="POST" action="signup.php">
		<div class="input-group">
	  		<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
	  		<input type="text" class="form-control" name="inputUsername" placeholder="Nom d'utilisateur">
		</div>
		&nbsp;
		<div class="input-group">
	  		<span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
	  		<input type="password" class="form-control" name="inputPassword" id="pass1" placeholder="Mot de passe">
		</div>
		&nbsp;
		<div class="input-group">
	  		<span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
	  		<input type="password" class="form-control" name="inputConfirmPassword" id="pass2" placeholder="Confirmez votre mot de passe">
		</div>
		&nbsp;
		<div>
			<button type="submit" class="btn btn-primary">M'enregistrer</button>

		</div>

	</form>

</div>
</body>
</html>
