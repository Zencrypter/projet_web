<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
    @include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Identification</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Identification <small>Veuillez vous identifier !</small></h1>
	</div>

	<!-- Displays an error message if the username / password is incorrect -->
	@if($var == 0)
	<div class="alert alert-danger" role="alert">
		<strong>Erreur !</strong> Identifiant et/ou mot de passe incorrect(s). Réessayez.
	</div>
	@endif
	@if($var == 2)
	<div class="alert alert-success" role="alert">
		<strong>Welcome ! !</strong> Votre compte à bien été crée.
	</div>
	@endif

	<!-- Form to sign in -->
	<form method="POST" action="login.php">
		<input type="hidden" name="inputID" value="0">
		<div class="input-group">
	  		<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
	  		<input type="text" class="form-control" name="inputUsername" placeholder="Nom d'utilisateur">
		</div>
		&nbsp;
		<div class="input-group">
	  		<span class="input-group-addon"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></span>
	  		<input type="password" class="form-control" name="inputPassword" placeholder="Mot de passe">
		</div>
		&nbsp;
		<div>
			<button type="submit" class="btn btn-primary">M'identifier</button>
		</div>
		
	</form>
		
	<form method="GET" action="signup.php">
		Pas encore de compte? Inscrivez vous ;)
		&nbsp;
		<button type="submit" class="btn btn-primary">Sign In</button>
	</form>

</div>
</body>
</html>
