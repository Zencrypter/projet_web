<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
	@include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Livre ajouté !</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Livre ajouté et bien reçu !</h1>
	</div>

	<!-- Display the added book -->
	&nbsp;
	<p>Nous vous annonçons que le livre qui a été ajouté (ou modifié) a bien été pris en compte.</p>
	&nbsp;
	<p>Détails :</p>
	<ul>
		<li>Titre : {{ $book->title }}</li>
		<li>Auteur : {{ $book->author_name }}</li>
		<li>Edition : {{ $book->edition }}</li>
		<li>Type : {{ $book->gender }}</li>
		<li>Date de publication : {{ $book->publication_date }}</li>
	</ul>
	&nbsp;
	
	<!-- Buttons to go back to the home or donnate another book -->
	<p>Que souhaitez-vous faire ?</p>
	
	<div class="row">
		<div class="col-sm-3">
			<form method="GET" action="home.php" style="text-align: center;">
				<button type="submit" class="btn btn-primary">Retourner à la page d'accueil</button>
			</form>
		</div>
		<div class="col-sm-2 col-sm-offset-2" style="text-align: center;">
			<form method="POST" action="/editbook.php">
				<input type="hidden" name="inputID" value="{{ $id }}">
				<button type="submit" class="btn btn-success">Modifier le livre saisi</button>
			</form>
		</div>
		<div class="col-sm-3 col-sm-offset-2" style="text-align: center;">
			<form method="GET" action="donnate.php">
				<button type="submit" class="btn btn-primary">Faire don d'un autre livre :)</button>
			</form>
		</div>
	</div>
	
</div>
</body>
</html>
