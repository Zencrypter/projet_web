<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
    @include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Formulaire de don</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Formulaire de don</h1>
	</div>

	<!-- Navigation Menu (navbar) -->
	@include('includes.navbar')

	<!-- Little paragraph -->
	<p>Vous trouverez ci-dessous un formulaire permettant de faire le don d'un livre à la Bibliothèque</p>
	<p>Toute l'équipe vous remercie par avance pour votre contribution !</p>
	
	&nbsp;


	<!-- Form to add a book -->
	<form method="POST" action="addbook.php">
	<div class="input-group">
  		<span class="input-group-addon">Titre</span>
  		<input type="text" class="form-control" name="inputTitle" placeholder="Les Misérables, Phèdre...">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Auteur</span>
  		<input type="text" class="form-control" name="inputAuthor" placeholder="Victor Hugo, Platon...">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Edition</span>
  		<input type="text" class="form-control" name="inputEdition" placeholder="Larousse, Folio...">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Type</span>
  		<input type="text" class="form-control" name="inputGender" placeholder="Théâtre, Roman...">
	</div>
	&nbsp;
	<div class="input-group">
  		<span class="input-group-addon">Date de publication</span>
  		<input type="text" class="form-control" name="inputDate" placeholder="JJ/MM/AAAA">
	</div>
	&nbsp;
	<div>
		<button type="submit" class="btn btn-primary">Envoyer !</button>
	</div>
	</form>

</div>
</body>
</html>
