<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
    @include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Bienvenue à la bibliothèque !</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Bienvenue à la bibliothèque !</h1>
	</div>

	<!-- Navigation Menu (navbar) -->
	@include('includes.navbar')
	
	<!-- Display the available books -->
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">Les 5 derniers ajouts à la bibliothèque :</div>

		<!-- Table -->
		<table class="table">
			<thead>
				<tr>
				<th>Titre</th>
				<th>Nom de l'auteur</th>
				<th>Edition</th>
				<th>Genre</th>
				</tr>
			</thead>
			<tbody>
			
			@foreach($latestbooks as $book)
			<tr>
				<td>{{ $book->title }}</td>
				<td>{{ $book->author_name }}</td>
				<td>{{ $book->edition }}</td>
				<td>{{ $book->gender }}</td>	
			</tr>
			@endforeach
			
			</tbody>
		</table>

	</div>

</div>
</body>
</html>
