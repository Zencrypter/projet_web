<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
	@include('includes.meta')
	<meta http-equiv="refresh" content="4;url=index.php" />
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Déconnexion</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Déconnexion </h1>
	</div>

	<!-- Goodbye message -->
	<p>Vous avez été déconnecté !</p>
	<p>La bibliothèque vous remercie pour votre visite.</p>
	&nbsp;
	<p>Vous allez être redirigé vers la page d'accueil dans 4s. Si cela n'est pas le cas, merci de <a href="index.php">cliquer ici</a>.</p>
	
	<!-- Wonderful progress bar -->
	<div class="progress">
  		<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
		</div>
	</div>
</div>
</body>
</html>
