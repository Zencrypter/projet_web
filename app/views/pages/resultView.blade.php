<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
    @include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Résultat de votre recherche</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Résultat de votre recherche</h1>
	</div>

	<!-- Navigation Menu (navbar) -->
	@include('includes.navbar')

	<!-- Display the search results -->
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">Livres actuellement disponibles :</div>

		<!-- Table -->
		<table class="table">
			<thead>
				<tr>
				<th>Titre</th>
				<th>Nom de l'auteur</th>
				<th>Edition</th>
				<th>Genre</th>
				<th>Emprunter</th>
				</tr>
			</thead>
			<tbody>
			
			@foreach($books as $book)
			<tr>
				<td>{{ $book->title }}</td>
				<td>{{ $book->author_name }}</td>
				<td>{{ $book->edition }}</td>
				<td>{{ $book->gender }}</td>
				<td>
					@if($book->userID == 0)
					<a href="home.php?id={{ $book->id }}" style="color: rgb(11,133,14)"><font color="0B850E">Emprunter</font></a>
					</form>
					@else
					<font color="FF0000">Indisponible</font>
					@endif
				</td>
			</tr>
			@endforeach
			
			</tbody>
		</table>

	</div>

</div>
</body>
</html>
