<!DOCTYPE html>
<html lang="fr">
<head>

	<!-- META Tags -->
    @include('includes.meta')
	
	<!-- CSS Import -->
	@include('includes.css')
	
	<!-- Tab Title -->
	<title>Bienvenue à la bibliothèque !</title>
	
</head>
<body>

<div class="container">

	<!-- Page Title -->
	<div class="page-header">
		<h1>Bienvenue à la bibliothèque !</h1>
	</div>

	<!-- Navigation Menu (navbar) -->
	@include('includes.navbar')
	
	<!-- Display the borrowed books -->
	<div class="panel panel-default">
	
		<!-- Default panel contents -->
		<div class="panel-heading">Mes livres empruntés :</div>

		<!-- Table -->
		<table class="table">
		
			<!-- If the user didn't borrow any book -->
			@if(empty($books))
			<tbody>
				<td align="center">Pas de livre emprunté !</tr>
			</tbody>

			<!-- Else, dispay the books -->
			@else
			<thead>
				<tr>
				<th>Titre</th>
				<th>Nom de l'auteur</th>
				<th>Edition</th>
				<th>Genre</th>
				</tr>
			</thead>
			<tbody>
			
			@foreach($books as $book)
			<tr>
				<td>{{ $book->title }}</td>
				<td>{{ $book->author_name }}</td>
				<td>{{ $book->edition }}</td>
				<td>{{ $book->gender }}</td>	
			</tr>
			@endforeach
			
			</tbody>
			
		@endif
		</table>

	</div>

</div>
</body>
</html>
