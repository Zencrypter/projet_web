<?php


class Book extends Eloquent {

	// Champs prot�g�s en �criture
	protected $fillable = array('title', 'author_name', 'edition', 'gender', 'publication_date');
	protected $table = "book";
	
}
