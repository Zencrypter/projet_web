<?php

class BookAppSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	
		// Delete the table book
		DB::table('book')->truncate();
		
		// Seed the table
		// Book 1 : Les Mis�rables
		$bookMiserables = Book::create(array(
			'title'			=> 'Les Miserables',
			'author_name'	=> 'Victor Hugo',
			'edition'		=> 'Edition 1',
			'gender'		=> 'Roman',
			'publication_date' => '2000-01-10',
			'added_date' => '2014-09-12'
		));
		
		// Book 2 : Les Contemplations
		$bookContemplations = Book::create(array(
			'title'			=> 'Les Contemplations',
			'author_name'	=> 'Victor Hugo',
			'edition'		=> 'Edition 2',
			'gender'		=> 'Roman',
			'publication_date' => '2000-08-05',
			'added_date' => '2014-09-23'
		));
		
		// Book 3 : Madame Bovary
		$bookMadameBovary = Book::create(array(
			'title'			=> 'Madame Bovary',
			'author_name'	=> 'Gustave Flaubert',
			'edition'		=> 'Folio Classique',
			'gender'		=> 'Roman',
			'publication_date' => '2001-05-16',
			'added_date' => '2014-10-01'
		));

		// Book 4 : Rhinoc�ros
		$bookRhinoceros = Book::create(array(
			'title'			=> 'Rhinoceros',
			'author_name'	=> 'Eugene Ionesco',
			'edition'		=> 'Folio Theatre',
			'gender'		=> 'Theatre',
			'publication_date' => '1999-03-03',
			'added_date' => '2014-10-07'
		));

		// Book 5 : B�r�nice
		$bookberenice = Book::create(array(
			'title'			=> 'Benerice',
			'author_name'	=> 'Racine',
			'edition'		=> 'Larousse',
			'gender'		=> 'Theatre',
			'publication_date' => '2011-08-17',
			'added_date' => '2014-10-14'
		));

		// Book 6 : Le Dimanche De La Vie
		$bookLeDimancheDeLaVie = Book::create(array(
			'title'			=> 'Le Dimanche De La Vie',
			'author_name'	=> 'Raymond Queneau',
			'edition'		=> 'Folio',
			'gender'		=> 'Roman',
			'publication_date' => '1973-09-14',
			'added_date' => '2014-10-23'
		));

		// Book 7 : Le Ravissement De Lol V.Stein
		$bookLeRavissementDeLolVStein = Book::create(array(
			'title'			=> 'Le Ravissement De Lol V.Stein',
			'author_name'	=> 'Marguerite Duras',
			'edition'		=> 'Folio Classique',
			'gender'		=> 'Roman',
			'publication_date' => '1976-06-25',
			'added_date' => '2014-10-29'
		));

		// Book 8 : Les Fleurs Du Mal
		$bookLesFleursDuMal = Book::create(array(
			'title'			=> 'Les Fleurs Du Mal',
			'author_name'	=> 'Charles Baudelaire',
			'edition'		=> 'Pocket',
			'gender'		=> 'Poesie',
			'publication_date' => '2009-09-03',
			'added_date' => '2014-11-01'
		));

		// Book 9 : Ph�dre
		$bookPhedre = Book::create(array(
			'title'			=> 'Phedre',
			'author_name'	=> 'Jean Racine',
			'edition'		=> 'Pocket',
			'gender'		=> 'Theatre',
			'publication_date' => '2005-06-23',
			'added_date' => '2014-11-05'
		));

		// Book 10 : Le Barbier De S�ville
		$bookLeBarbierDeSeville = Book::create(array(
			'title'			=> 'Le Barbier de Seville',
			'author_name'	=> 'Beaumarchais',
			'edition'		=> 'Folio Classique',
			'gender'		=> 'Theatre',
			'publication_date' => '2007-03-29',
			'added_date' => '2014-11-17'
		));
	}

}
