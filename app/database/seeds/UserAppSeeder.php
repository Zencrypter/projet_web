<?php

class UserAppSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	
		// Deletes the table book
		DB::table('users')->truncate();
		
		// Seeds the admin account
		$userAdmin = User::create(array(
			'username'	=> 'admin',
			'password'	=> Hash::make('admin')
		));
	
		// Seeds the user1 account
		$userAdmin = User::create(array(
			'username'	=> 'user1',
			'password'	=> Hash::make('user1')
		));
		
		// Seeds the user2 account
		$userAdmin = User::create(array(
			'username'	=> 'user2',
			'password'	=> Hash::make('user2')
		));
	}

}
