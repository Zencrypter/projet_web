<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('BookAppSeeder');
		$this->command->info('book app seeds finished');
		$this->call('LoansSeeder');
		$this->command->info('loans seeds finished');
		$this->call('UserAppSeeder');
		$this->command->info('user app seeds finished');
	}

}
