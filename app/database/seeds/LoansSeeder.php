<?php

class LoansSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
	
		// Deletes the table loans
		DB::table('loans')->truncate();
		
		// Adds each book as unborrowed in the database
		for($i = 1 ; $i <= 10 ; $i++) {
		
			// Creates the loan entry
			Loan::create(array(
				'bookID'	=> $i,
				'userID'	=> 0
			));
		
		}
		
	}

}
