<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Book extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('book', function(Blueprint $table)
		{
			// Creates the columns
			$table->increments('id');
			$table->string('title');
			$table->string('author_name');
			$table->string('edition');
			$table->string('gender');			
			$table->date('publication_date');
			$table->date('added_date');
			$table->timestamps();
			
			// Constraints
			// $table->primary('id');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('book');
	}

}
