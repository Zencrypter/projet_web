<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			// Creates the columns
			$table->increments('id');
			$table->string('username');
			$table->string('password');
			$table->text('remember_token')->nullable;
			$table->timestamps();
			
			// Constraints
			// $table->primary('id');
			// $table->unique('username');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
