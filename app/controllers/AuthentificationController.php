<?php

class AuthentificationController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|
	*/
	
	
	// Function to login to access the blog
	public function loginGet() {
		
		$var = 0;
		return View::make('pages/loginView')->with('var', 1);
	
	}

	// Function called when the users logs in
	public function loginPost() {

		$userdata = array('username' => $_POST['inputUsername'],'password' => $_POST['inputPassword']);
		
		// If the authentification is successful
		if (Auth::attempt($userdata)) {
			
			// Shares the username of the connected guy
			$user_connected = $_POST['inputUsername'];
			
			// Redirects to the home page
			return Redirect::to('home.php');

		}
		
		// Else, goes back to the login page with an error
		else
			return View::make('pages/loginView')->with('var', 0);
			
	}


	// Function to get the "sign up" View
	public function signupGet() {
		$var = 0;
		return View::make('pages/signupView')->with('var', 1);
	}


	// Function to create a new user
	public function signupPost() {

		$Pass = $_POST['inputPassword'];
		$Cpass = $_POST['inputConfirmPassword'];

		if($Pass == $Cpass){

			$new_user = array(
				'username' => $_POST['inputUsername'],
				'password' => Hash::make($Pass)
			);
			
			// Insert the new user in the table
			DB::table('users')->insert($new_user);
			return View::make('pages/loginView')->with('var', 2);
		}
		
		else{
			return View::make('pages/signupView')->with('var', 0);
		}			
	}

	// Function to logout
	public function logout() {

		// Logs the user out
		Auth::logout();
		
		// Returns the logout view
		return View::make('pages/logoutView');
	
	}

}
