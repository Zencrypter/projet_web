<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	// Search page: Find and borrow books from the library
	public function search() {
	
		// Call the view
		return View::make('pages/searchView');
	}
	
	// Donnation page: donnate a book to the library
	public function donnate() {
	
		// Call the view
		return View::make('pages/donnateView');
	}

	// Result of the search page
	public function result() {
	
		// Call the view
		return View::make('pages/resultView');
	}

	// Function to modify a book which has just been added
	public function editbook() {
	
		// Call the view
		return View::make('pages/editBookView');
	
	}
}
