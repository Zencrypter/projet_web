<?php

class APIController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	| Info here: http://laravel.com/docs/4.2/controllers#restful-resource-controllers
	|
	*/

	// GET: Displays the books in the library
	// Test with: curl --user admin:admin 127.0.0.1/api/v1
	public function index() {
	
		// Retrieves the books
		$books = DB::table('book')->get();
			
		// Returns the result
		return Response::json(array(
			'error' => false,
			'books' => $books,
			200
		));
	}

	// GET: Displays a particular books in the library
	// Test with: curl --user admin:admin 127.0.0.1/api/v1/1
	public function show($id) {
	
		// Retrieves the books
		$book = DB::table('book')
			->where('id', $id)
			->get();
			
		// Returns the result
		return Response::json(array(
			'error' => false,
			'book' => $book,
			200
		));
	}

	// POST: Donates a book to the library
	// Test with: curl -i --user admin:admin -d 'title=Trololo&author=Eduard Khil&edition=Trolledition&gender=fun&publication_date=2011-07-12' 127.0.0.1/api/v1
	public function store() {
	
		// Retrieves the parameters given
		$new_book = array(
			'title' => Request::get('title'),
			'author_name' => Request::get('author'),
			'edition' => Request::get('edition'),
			'gender' => Request::get('gender'),
			'publication_date' => Request::get('publication_date'),
			'added_date' => date('Y-m-d')
		);
		
		// Inserts the new book in the database
		$id = DB::table('book')->insertGetId($new_book);
		
		// Returns the new book given
		return Response::json(array(
			'error' => false,
			'book' => $new_book,
			'id' => $id,
			200
		));
	}
	
	// UPDATE: Updates a book in the library
	// Test with: curl -i -X PUT --user admin:admin -d 'title=Trololo&author=Eduard Khil&edition=Trolledition&gender=fun&publication_date=2011-07-12' 127.0.0.1/api/v1/1
	public function update($id) {
	
		// Retrieves the updated info
		$book = array(
			'title' => Request::get('title'),
			'author_name' => Request::get('author'),
			'edition' => Request::get('edition'),
			'gender' => Request::get('gender'),
			'publication_date' => Request::get('publication_date'),
			'added_date' => date('Y-m-d')
		);
		
		// Updates the book in the database and returns the new data
		DB::table('book')
			->where('id', $id)
            ->update($book);
            
        $updated_book = DB::table('book')
			->where('id', $id)
            ->get();    
	
		// Returns the updated book given
		return Response::json(array(
			'error' => false,
			'book' => $updated_book,
			200
		));
	}
	
	
	// DELETE: Deletes a book from the library
	// Test with: curl -i -X DELETE --user user1:user1 127.0.0.1/api/v1/1
	// And curl -i -X DELETE --user admin:admin 127.0.0.1/api/v1/2
	public function destroy($id) {
	
		// If the user is "admin"
		if (Auth::user()->username == 'admin') {
	
			// Deletes the book in the database
			DB::table('book')
				->where('id', $id)
				->delete();

			// Returns the new book given
			return Response::json(array(
				'error' => false,
				200
			));
		
		}
	
		// Else, returns an error
		else {
	
			return Response::json(array(
				'error' => true,
				403
			));
		
		}
	
	}
	
}
