<?php

class DatabaseController extends BaseController {

	// Index page
	public function index() {
		
		// Call the Database
		$latestbooks = DB::table('book')->take(5)->orderBy('added_date', 'desc')->get();
		
		// Call the view
		return View::make('pages/indexView')->with('latestbooks', $latestbooks);;
	}


	// Home page: New books, links to the pother sections
	public function home() {
	
		// Borrows the book if the user comes from "search & borrow"
		if(!empty($_GET['id'])) {
		
			// Borrows the book
			DB::table('loans')
				->where('bookID', $_GET['id'])
		        ->update(array('userID' => Auth::user()->id));
		
		}
		
		// Call the Database
		$borrowedbooks = DB::table('book')
			->join('loans','loans.bookID','=','book.ID')
			->where('loans.userID', Auth::user()->id)
			->get();
		
		// Call the view
		return View::make('pages/homeView')
			->with('var', 1)
			->with('books', $borrowedbooks);
	}


	// Function to add a book in the database
	public function addbook() {
	
		// Convert the date for MySQL
		$date = explode("/", $_POST['inputDate']);
		$mysqldate = $date[2].'-'.$date[1].'-'.$date[0];
	
		// Create an array from the POST data
		$new_book = array(
			'title' => $_POST['inputTitle'],
			'author_name' => $_POST['inputAuthor'],
			'edition' => $_POST['inputEdition'],
			'gender' => $_POST['inputGender'],
			'publication_date' => $mysqldate,
			'added_date' => date('Y-m-d')
		);
	
		// Insert the new book in the table and gets its id
		$id = DB::table('book')->insertGetId($new_book);
		
		// Add the new book in the loans table
		DB::table('loans')->insert(array(
			'bookID'=> $id,
			'userID'=> 0
		));
		
		// Call the addedbook view to tell the submitter the book has been added
		return View::make('pages/addedBookView')
			->with('id', $id)
			->with('book', (object)$new_book);

	}


	// Function to edit a book
	public function editbook() {
	
		// Get the ID from the URL
		$id = $_POST['inputID'];
	
		// Retrieve the references of the book
		$book_ = DB::table('book')->where('id', $id)-> get();
		$book = (object) $book_[0];
		
		// Convert the date for MySQL
		$mysqldate = explode("-", $book->publication_date);
		$date = $mysqldate[2].'/'.$mysqldate[1].'/'.$mysqldate[0];
		
		// Modify the date in the book variable
		$book->publication_date = $date;
		
		// Call the addedbook view to tell the submitter the book has been added
		return View::make('pages/editBookView')
			->with('id', $id)
			->with('book', $book);

	}


	// Function to modify a book which has just been added
	public function applychanges() {
	
		// Convert the date for MySQL
		$date = explode("/", $_POST['inputDate']);
		$mysqldate = $date[2].'-'.$date[1].'-'.$date[0];
	
		// Create an array from the POST data
		$book = array(
			'title' => $_POST['inputTitle'],
			'author_name' => $_POST['inputAuthor'],
			'edition' => $_POST['inputEdition'],
			'gender' => $_POST['inputGender'],
			'publication_date' => $mysqldate
		);
		
		// Updates the book in the database
		DB::table('book')
			->where('id', $_POST['inputID'])
            ->update($book);
		
		// Call the addedbook view to tell the submitter the book has been added
		return View::make('pages/addedBookView')
			->with('id', $_POST['inputID'])
			->with('book', (object)$book);
	}


	// Deletion page with GET: display the database
	public function deleteget() {
	
		if (Auth::user()->username == 'admin') {
		
			// Call the Database
			$books = DB::table('book')->get();
	
			// Call the view
			return View::make('pages/deleteView')->with('books', (object)$books);
		
		}
		
		// Else, redirects to the home page
		else
			return Redirect::to('home.php');
			
	}

	// Deletion page with POST: delete a book and display the database
	public function deletepost() {
	

    	// If the user is admin...
		if (Auth::user()->username == 'admin') {

			// Delete the book
			DB::table('book')
				->where('id', $_POST['inputID'])
				->delete();
	
			// Delete the book in the loans table
			DB::table('loans')->insert(array(
				'bookID'=> $_POST['inputID'],
				'userID'=> 0
			));
	
			// Call the Database
			$books = DB::table('book')->get();
	
			// Call the view
			return View::make('pages/deleteView')->with('books', (object)$books);
		}
		
		// Else, redirects to the home page
		else 
			return Redirect::to('home.php');

	}

	// Function to retrieve the books corresponding to the search
	public function searchBook() {

		// Database query
		$result = DB::table('book')
			->join('loans','loans.bookID','=','book.ID')
			->where('book.title', $_POST['inputTitle'])
			->orwhere('book.author_name', $_POST['inputAuthor'])
			->orwhere('book.edition', $_POST['inputEdition'])
			->orwhere('book.gender', $_POST['inputGender'])
			->get();
		
		// Return the view
		return View::make('pages/resultView')
			->with('current_userID', Auth::user()->id)
			->with('books', $result);
	}
	
	// Function to return a book
	public function returnbook() {
	
		// Returns the book if the user clicked on the link
		if(!empty($_GET['id'])) {
		
			// Returns the book
			DB::table('loans')
				->where('bookID', $_GET['id'])
		        ->update(array('userID' => 0));
		
		}
	
		// Call the Database
		$borrowedbooks = DB::table('book')
			->join('loans','loans.bookID','=','book.ID')
			->where('loans.userID', Auth::user()->id)
			->get();
	
		// Call the view
		return View::make('pages/returnBookView')->with('books', $borrowedbooks);
	}
	
	
}
