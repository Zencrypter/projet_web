Système de gestion de librairie
===============================

Projet de développement web à l'ESIEA Paris, année universitaire 2014/2015.


# Features

* Écran d'accueil simple affichant les 5 derniers livres ajoutés à la bibliothèque.
* Gestion des utilisateurs (pseudonyme + mot de passe).
* Gestion de l'authentification : toute page (en dehors de la page d'accueil) est inaccessible tant que l'utilisateur n'est pas connecté.
* Gestion complète d'un système de librairie :
	* Possibilité de donner un livre par le biais de la page "Don".
	* Possibilité de rechercher un livre dans la base de données à partir de son nom, de l'auteur... depuis la page "Rechercher et emprunter"
	* Possibilité d'emprunter un livre (après recherche) ou de rendre un livre (page "Retour") en un clic.
	* Possibilité de supprimer un livre de la base de données pour l'**administrateur** uniquement (cf. en fin de document pour les identifiants).
* API RESTful permettant d'exploiter les données du système depuis une application mobile par exemple (pour les tester, cf fichier APIController.php).


# Notes de développement

* Le modèle MVC a été respecté tout au long du développement.
* Un effort a été fait vis-à-vis de la conception du modèle relationnel de la base de données créée.
* Utilisation de Bootstrap dans tout le site web, permettant un joli rendu.
* Utilisation de Laravel, Blade et Eloquent comme demandé.


# Comptes déjà prêts

* Comptes utilisateur : user1/user1 et user2/user2
* Compte administrateur : admin/admin
